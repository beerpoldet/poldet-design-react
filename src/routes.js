import HomePage from './pages/HomePage'
import InboxPage from './pages/InboxPage'

export default {
  "/": HomePage,
  "/inbox": InboxPage,
}