import 'typeface-roboto'

import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom'
import LoginPage from './pages/LoginPage'
import MainApp from './templates/App/MainApp'

class AppRouter extends Component {
  render() {
    const { mainAppRoutes } = this.props
    return (
      <Router>
        <Switch>
          <Route path="/login" component={LoginPage} />
          <MainApp routes={mainAppRoutes} />
        </Switch>
      </Router>
    );
  }
}

export default AppRouter;
