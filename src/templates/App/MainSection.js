import React from 'react';
import SectionWithMenuAppBar from '../../views/Section/SectionWithMenuAppBar'
import DrawerMenu from '../common/DrawerMenu'
import HomeButton from '../common/HomeButton'
import ResponsiveDrawer from '../../views/Drawer/ResponsiveDrawer';

export default class MainSection extends React.Component {

  toolbar = () => (
    <HomeButton />
  )

  drawer = () => (
    <DrawerMenu />
  )

  render() {
    return (
      <SectionWithMenuAppBar
        drawer={this.drawer()}
        toolbar={this.toolbar()}>
        {this.props.children}
      </SectionWithMenuAppBar>
    )
  }
}