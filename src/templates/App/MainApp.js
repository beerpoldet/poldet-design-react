import React from 'react'
import {
  Switch,
  Route,
} from 'react-router-dom';
import { map } from 'lodash'
import MainSection from './MainSection'

export default ({ routes }) => (
  <div>
    <MainSection>
      <Switch>
        {map(routes, (page, path) => (
          <Route
            key={path}
            path={path}
            component={page}
            exact />
        ))}
      </Switch>
    </MainSection>
  </div>
) 