import React from 'react'
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid'
import Hidden from 'material-ui/Hidden'
import InboxListContainer from './InboxListContainer'
import MailViewContainer from './MailViewContainer'

const InboxSplitView = ({
  classes,
  viewingEmail,
  onEmailClick,
}) => {
  return (
    <Grid container spacing={0}>
      <Grid item xs={12} sm={4}>
        <Paper className={classes.fullHeight}>
          <Typography type="title" className={classes.header}>
            Inbox
          </Typography>
          <InboxListContainer 
            onEmailClick={onEmailClick} />
        </Paper>
      </Grid>
      <Hidden only={["xs"]}>
        <Grid item xs={8}>
          <Paper className={classes.fullHeight}>
            <MailViewContainer
              email={viewingEmail} />
          </Paper>
        </Grid>
      </Hidden>
    </Grid>
  )
}

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  fullHeight: {
    height: `calc(100vh - 64px)`
  },
  header: {
    paddingTop: theme.spacing.unit * 2,
    paddingLeft: theme.spacing.unit * 3,
    paddingRight: theme.spacing.unit * 3,
    paddingBottom: theme.spacing.unit,
  }
})

export default withStyles(styles)(InboxSplitView);