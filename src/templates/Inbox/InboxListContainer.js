import React from 'react'
import InboxList from './InboxList'

const emails = [
  {
    id: 1,
    header: "Please send us more infomation",
    sender: {
      name: "Poldet Assanangkornchai",
      short: "PD",
    },
    sentTime: "10:07 PM",
    content: `
    With FreshBooks, you can easily automate tedious tasks. From expense tips to invoice tricks, here are a few ways that FreshBooks helps you save time:
    Recurring Invoices 
    Do you have a client that you bill frequently? FreshBooks can auto-generate invoices so you don't have to lift a finger.
    `
  }
]

// MARK: this should fetch emails from server api

export default class InboxListContainer extends React.Component {

  render() {
    return (
      <InboxList
        emails={this.props.emails || emails}
        onEmailClick={this.props.onEmailClick} />
    )
  }
}