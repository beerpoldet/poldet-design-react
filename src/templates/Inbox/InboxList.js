import React from 'react'
import List, {
  ListItem,
  ListItemIcon,
  ListItemText,
} from 'material-ui/List'
import Typography from 'material-ui/Typography'
import MailOutlineIcon from 'material-ui-icons/MailOutline'
import { withStyles } from 'material-ui/styles'
import classNames from 'classnames'

const InboxList = (props) => (
    <List>
      {renderEmails(props)}
    </List>
  )

const renderEmails = ({
  classes,
  emails,
  onEmailClick,
}) => {
  if (!emails)
    return (
      <Typography
        type="subheading"
        align="center"
        className={classNames(classes.row, classes.emptyState)}>
        <MailOutlineIcon className={classes.icon} /> No email
      </Typography>
    )
  return (
    emails.map(email => (
      <ListItem
        key={email.id}
        onClick={() => onEmailClick(email)}
        button>
        <ListItemText
          primary={email.header}
          secondary={email.sender.name} />
      </ListItem>
    ))
  )
}

const styles = theme => ({
  emptyState: {
    marginTop: theme.spacing.unit * 3
  },
  row: {
    display: 'flex',
    justifyContent: 'center'
  },
  icon: {
    marginRight: 10,
  }
})

export default withStyles(styles)(InboxList)