import React from 'react'
import InboxSplitView from './InboxSplitView'

export default class InboxSplitViewContainer extends React.Component {
  state = {
    viewingEmail: undefined,
  }

  handleEmailClick = email => {
    this.setState(state => ({
      ...state,
      viewingEmail: email
    }))
  }

  render() {
    return (
      <InboxSplitView
        viewingEmail={this.state.viewingEmail}
        onEmailClick={this.handleEmailClick} />
    )
  }
}