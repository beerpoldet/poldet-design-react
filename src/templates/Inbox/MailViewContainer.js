import React from 'react'
import MailView from './MailView'

// MARK: this component will fetch more information of viewing email

export default class MailViewContainer extends React.Component {
  render() {
    return (
      <MailView
        email={this.props.email} />
    )
  }
}