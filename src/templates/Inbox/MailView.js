import React from 'react'
import Typolography from 'material-ui/Typography'
import { withStyles } from 'material-ui/styles'
import MailIcon from 'material-ui-icons/Mail'
import Avatar from 'material-ui/Avatar'
import blue from 'material-ui/colors/blue';
import Grid from 'material-ui/Grid'
import AvatarInfo from '../../views/Content/AvatarInfo'

class MailView extends React.Component {
  render() {
    const { classes, email } = this.props
    if (!email)
      return renderEmptyState(classes)

    const { header, sender, content, sentTime } = email
    return (
      <div className={classes.root}>
        <Typolography type="headline" className={classes.header}>
          {header}
        </Typolography>
        
        <AvatarInfo
          avatarCode={sender.short}
          primary={sender.name}
          secondary={sentTime} />
          
        <Typolography type="body1" className={classes.body}>
          {content}
        </Typolography>
      </div>
    )
  }
}

const styles = theme => ({
  root: {
    padding: theme.spacing.unit * 3
  },
  header: {
    paddingBottom: theme.spacing.unit * 3
  },
  body: {
    paddingTop: theme.spacing.unit * 2
  }
})

const renderEmptyState = (classes) => (
  <Typolography
    className={classes.root}
    type="title">
    No email selected
  </Typolography>
)

export default withStyles(styles)(MailView)