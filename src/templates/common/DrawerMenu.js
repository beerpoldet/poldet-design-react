import React from 'react'
import { withStyles } from 'material-ui/styles'
import List, {
  ListItem,
  ListItemIcon,
  ListItemText,
} from 'material-ui/List'
import InboxIcon from 'material-ui-icons/Inbox'
import { Link } from 'react-router-dom'

const DrawerMenu = ({ classes, ...props }) => {
  return (
    <List>
      <Link to="/inbox">
        <ListItem button>
          <ListItemIcon>
            <InboxIcon />
          </ListItemIcon>
          <ListItemText primary="Inbox" />
        </ListItem>
      </Link>
    </List>
  )
}

const styles = {
}

export default withStyles(styles)(DrawerMenu)