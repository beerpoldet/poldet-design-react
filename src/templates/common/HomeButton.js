import React from 'react'
import Typography from 'material-ui/Typography'
import { withStyles } from 'material-ui/styles'
import { Link } from 'react-router-dom'

const styles = theme => ({
  flex: { flext: 1 }
})

const HomeButton = (classes) => (
  <Link to="/">
    <Typography
      type="title"
      color="inherit"
      className={classes.flex}>
      Poldet Design
    </Typography>
  </Link>
)

export default withStyles(styles)(HomeButton)