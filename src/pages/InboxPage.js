import React from 'react'
import InboxSplitViewContainer from '../templates/Inbox/InboxSplitViewContainer'

const InboxPage = () => {
  return (
    <div>
      <InboxSplitViewContainer />
    </div>
  )
}

export default InboxPage;