import React from 'react'
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';

const HomePage = ({ classes }) => {
  return (
    <div>
      <div className={classes.container}>
        <Paper className={classes.root} elevation={4}>
          <Typography type="headline" component="h3">
            Welcome to Poldet Design
          </Typography>
          <Typography type="body1" component="p">
            this application is to demonstrate Poldet design with ReactJS
          </Typography>
        </Paper>
      </div>
    </div>
  )
}

const styles = theme => ({
  container: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
  }),
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: theme.spacing.unit * 2,
  }),
})

export default withStyles(styles)(HomePage);