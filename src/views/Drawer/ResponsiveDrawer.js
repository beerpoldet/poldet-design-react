import React from 'react'
import { withStyles } from 'material-ui/styles';
import Drawer from 'material-ui/Drawer'
import Hidden from 'material-ui/Hidden'
import Divider from 'material-ui/Divider'

const drawerWidth = 250

const ResponsiveDrawer = ({
  classes,
  children,
  isDrawerOpen,
  onDrawerClose,
}) => (
    <div>
      <Hidden mdUp>
        <Drawer
          type="temporary"
          anchor='left'
          open={isDrawerOpen}
          classes={{
            paper: classes.drawerPaper,
          }}
          onRequestClose={onDrawerClose}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}>
          <div>
            <div className={classes.drawerHeader} />
            <Divider />
            <div onClick={onDrawerClose}>
              {children}
            </div>
          </div>
        </Drawer>
      </Hidden>

      <Hidden mdDown implementation="css">
        <Drawer
          type="permanent"
          open
          classes={{
            paper: classes.drawerPaper,
          }}>
          <div>
            <div className={classes.drawerHeader} />
            <Divider />
            {children}
          </div>
        </Drawer>
      </Hidden>
    </div>
  )

const styles = theme => ({
  drawerHeader: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
    [theme.breakpoints.up('md')]: {
      width: drawerWidth,
      position: 'relative',
      height: '100vh',
    },
  },
})

export default withStyles(styles)(ResponsiveDrawer)