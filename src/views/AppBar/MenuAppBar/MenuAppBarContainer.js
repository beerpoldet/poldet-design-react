import React from 'react'
import MenuAppBar from './MenuAppBar'

export default class MenuAppBarContainer extends React.Component {
  state = {
    isDrawerOpen: false,
  }

  toggleDrawer = isDrawerOpen => () => {
    this.setState(state => ({
      ...state,
      isDrawerOpen,
    }))
  }

  render() {
    return (
      <MenuAppBar
        {...this.props}
        isDrawerOpen={this.state.isDrawerOpen}
        onDrawerOpen={this.toggleDrawer(true)}
        onDrawerClose={this.toggleDrawer(false)} />
    )
  }
}