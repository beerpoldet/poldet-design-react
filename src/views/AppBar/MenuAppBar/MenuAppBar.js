import React from 'react';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import IconButton from 'material-ui/IconButton'
import MenuIcon from 'material-ui-icons/Menu'
import ResponsiveDrawer from '../../Drawer/ResponsiveDrawer'
import classNames from 'classnames'

const drawerWidth = 250

const styles = theme => ({
  appBar: {
    position: 'absolute',
    marginLeft: drawerWidth,
    [theme.breakpoints.up('md')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    },
  },
  navIconHide: {
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  menuButton: {
    marginRight: 20,
  },
})

const MenuAppBar = ({
  classes,
  theme,
  isDrawerOpen,
  onDrawerOpen,
  onDrawerClose,
  drawer,
  ...props,
}) => (
    <div>
      <AppBar
        className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="contrast"
            aria-label="Menu"
            className={classNames(
              classes.navIconHide,
              classes.menuButton
            )}
            onClick={onDrawerOpen}>
            <MenuIcon />
          </IconButton>
          {props.children}
        </Toolbar>
      </AppBar>

      <ResponsiveDrawer
        isDrawerOpen={isDrawerOpen}
        onDrawerClose={onDrawerClose}>
        {drawer}
      </ResponsiveDrawer>
    </div>
  )


export default withStyles(styles, { withTheme: true })(MenuAppBar)