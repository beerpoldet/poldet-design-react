import React from 'react'
import Typolography from 'material-ui/Typography'
import { withStyles } from 'material-ui/styles'
import Avatar from 'material-ui/Avatar'
import blue from 'material-ui/colors/blue';
import Grid from 'material-ui/Grid'

const AvatarInfo = ({
  classes,
  avatarCode,
  primary,
  secondary,
}) => (
    <Grid container className={classes.info}>
      <Grid item>
        <Avatar className={classes.avatar}>
          {avatarCode}
        </Avatar>
      </Grid>
      <Grid item>
        <Typolography type="subheading">
          {primary}
        </Typolography>
        <Typolography type="caption">
          {secondary}
        </Typolography>
      </Grid>
    </Grid>
  )

const styles = theme => ({
  info: {
    margin: 0
  },
  avatar: {
    color: '#fff',
    backgroundColor: blue[500],
  },
})

export default withStyles(styles)(AvatarInfo)