import React from 'react';
import { withStyles } from 'material-ui/styles';
import MenuAppBar from '../AppBar/MenuAppBar';

const styles = theme => ({
  appFrame: {
    position: 'relative',
    display: 'flex',
    width: '100%',
    height: '100vh',
  },
  content: {
    backgroundColor: theme.palette.background.default,
    width: '100%',
    height: 'calc(100% - 56px)',
    marginTop: 56,
    [theme.breakpoints.up('sm')]: {
      height: 'calc(100% - 64px)',
      marginTop: 64,
    },
  },
})

const SectionWithMenuAppBar = ({
  classes,
  toolbar,
  drawer,
  children,
  ...props,
}) => {
  return (
    <div className={classes.appFrame}>
      <MenuAppBar
        drawer={drawer}>
        {toolbar}
      </MenuAppBar>

      <main className={classes.content}>
        {children}
      </main>
    </div>
  )
}

export default withStyles(styles)(SectionWithMenuAppBar)