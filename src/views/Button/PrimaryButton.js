import React from 'react';
import Button from 'material-ui/Button';

export default (props) => {
  return (
    <Button raised color="primary">
      {props.children}
    </Button>
  );
}