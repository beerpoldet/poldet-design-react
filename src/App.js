import 'typeface-roboto'

import React, { Component } from 'react';
import AppRouter from './AppRouter'
import { MuiThemeProvider } from 'material-ui/styles'
import theme from './theme'
import routes from './routes'

class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <AppRouter mainAppRoutes={routes} />
      </MuiThemeProvider>
    );
  }
}

export default App;